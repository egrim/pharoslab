package pharoslabut.robotperimeter;

public class TargetSighting {

    public int hostId;
    public int targetId;
    public double timestamp;
    public double slope;
    public double yIntercept;
    public LocationStamp targetLocation;

}
