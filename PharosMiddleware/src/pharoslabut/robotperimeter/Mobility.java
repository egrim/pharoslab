package pharoslabut.robotperimeter;

import java.util.ArrayList;

import edu.utexas.ece.mpc.context.ContextHandler;
import edu.utexas.ece.mpc.context.summary.GroupContextSummary;
import pharoslabut.logger.Logger;
import pharoslabut.navigate.Location;
import pharoslabut.navigate.MotionArbiter;
import pharoslabut.navigate.NavigateCompassGPS;
import pharoslabut.sensors.CompassDataBuffer;
import pharoslabut.sensors.GPSDataBuffer;
import playerclient3.structures.gps.PlayerGpsData;

public class Mobility {
	MotionArbiter motionArbiter;
	CompassDataBuffer compassDataBuffer;
	GPSDataBuffer gpsDataBuffer;
	static NavigateCompassGPS nav;
	Intelligence intel;
//	Location fakeLocation; //either fake goal or own fake location (depending on type of robot)
//	boolean isSource = false;

	public Mobility(MotionArbiter motionArbiter, CompassDataBuffer compassDataBuffer,
			GPSDataBuffer gpsDataBuffer, int numTargets, boolean goToTarget, double forceField, boolean goToGoal, double goalLat, double goalLong, double finalPositionTolerance, boolean isTargetSource, int targetNumber, boolean isStatic/*, WiFiBeaconBroadcaster w*/)
	{
		this.motionArbiter = motionArbiter;
		this.compassDataBuffer = compassDataBuffer;
		this.gpsDataBuffer = gpsDataBuffer;
		nav = new NavigateCompassGPS(motionArbiter, compassDataBuffer, gpsDataBuffer);
		Logger.log("numTargets: " + numTargets);
		Location goalLocation = new Location (goalLat, goalLong);
//		this.fakeLocation = goalLocation;
		if (isTargetSource) intel = new TargetIntelligence(isStatic, targetNumber, 1);
		else intel = new Intelligence(0,10,10,10,20,10,0,0, numTargets, goToTarget, forceField, goToGoal, goalLocation, finalPositionTolerance);

		Logger.log("Created Mobility\n");
	}

	public static Location current;

	public void controlMotionAndIntelligence()
	{
		Logger.log("Started Motion control");
		long time = System.currentTimeMillis();
		int i=0;
		long currentTime = time;
		current = null;

		class GoThread extends Thread {

			Location start;
			Location goal;
			double speed;

			public GoThread(Location start, Location goal, double speed){
				this.start = start;
				this.goal = goal;
				this.speed = speed;
			}
			public void run() {
				nav.go(null, goal, speed);
			}
		}

		GoThread go = null;
		ArrayList<Location> pastLocations = new ArrayList<Location>(); //for GPS averaging
		while (true)
		{
			Logger.log("beginning of infinite loop");

			currentTime = System.currentTimeMillis();

			Location newLocation = null;
			try{
				PlayerGpsData newLoc = nav.getLocation();// nav.getCurrLoc();
				if(newLoc!=null){
					newLocation = new Location(newLoc);
					pastLocations.add(newLocation);
					if (pastLocations.size()>5) pastLocations.remove(0);
				}
			}
			catch(Exception e)
			{
				Logger.log(e.toString());
				Logger.log(e.getMessage());
			}
			if (newLocation !=null)		//	 current = newLocation;
			{
				double lat = 0;
				double longit = 0;
				double elev = 0;
				for (Location loc: pastLocations){
					lat+=loc.latitude();
					longit+=loc.longitude();
					elev+=loc.elevation();
				}
				lat/=pastLocations.size();
				longit/=pastLocations.size();
				elev/=pastLocations.size();

				current = new Location(lat,longit,elev);
			}

//			if (current==null)
//			current = new Location (30.28686686584115 + Math.random()*10, -97.73650601506233 + Math.random()*10, 0);
//			else{
//				current = new Location(current.latitude() + (Math.random()-.5)*5, current.longitude() + (Math.random()-.5)*5);
//			}

			if (current == null){
				Logger.log("no location known...continuing\n");
				try{
					Thread.sleep(1000);
				}
				catch(Exception e){}
				continue;
			}
			intel.updateLocationKnowledge(current);
			Logger.log("exited UpdateLocationKnowledge");
			Location goal = intel.determineNewGoPosition();
			double speed = intel.setSpeed();
			Logger.log("speed calculated based on distance: " + speed);
			if (go!=null)go.stop(); //TODO Find out if can do this safer
			go = new GoThread(current,goal,speed);
			go.start();
			ContextHandler handler = ContextHandler.getInstance();
//			Logger.log("Groups: ");
//			for (GroupContextSummary gr : handler.getGroupSummaries())
//				if (gr!=null)
//					Logger.log(gr.toString());
//				else Logger.log("null");
			Logger.log("printing summary of Intelligence loop: ");
			if (current!=null)
				Logger.log(String.format("Intelligence Done...Starting Again; current location %f %f; goal: %f %f",current.latitude(), current.longitude(), goal.latitude(), goal.longitude()));
			else 	Logger.log(String.format("Intelligence Done...Starting Again; current location null; goal: %f %f", goal.latitude(), goal.longitude()));

			try{
				Thread.sleep(3000);
			}
			catch(Exception e){
				Logger.log(e.toString());
			}
		}
	}
}
