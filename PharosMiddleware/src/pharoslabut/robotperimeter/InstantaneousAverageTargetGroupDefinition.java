package pharoslabut.robotperimeter;

import java.util.List;

import pharoslabut.navigate.Location;
import edu.utexas.ece.mpc.context.ContextHandler;
import edu.utexas.ece.mpc.context.group.LabeledGroupDefinition;
import edu.utexas.ece.mpc.context.summary.ContextSummary;
import edu.utexas.ece.mpc.context.summary.GroupContextSummary;
import edu.utexas.ece.mpc.context.summary.HashMapContextSummary;


public class InstantaneousAverageTargetGroupDefinition extends LabeledGroupDefinition {

	public InstantaneousAverageTargetGroupDefinition(int gId) {
		super(gId);
	}
	
	//aggregation: include an average of all the latest individual location samples available in a single outgoing group summary
	
    public void handleContextSummary(GroupContextSummary currentGroupSummary,
            ContextSummary newSummary)
    {
    	super.handleContextSummary(currentGroupSummary, newSummary);
    	//loop through all received summaries, retrieve target sightings, average them and store averaged value in group
    	
    	if (!(currentGroupSummary instanceof HashMapContextSummary)) return; //TODO
    	ContextHandler handler = ContextHandler.getInstance();
    	List<ContextSummary> allSummaries = handler.getReceivedSummaries();
    	
    	allSummaries.add(Intelligence.mySummary); //TODO determine other way to access localSummary (through ContextHandler?
    	
    	LocationStamp newLocationStamp = new LocationStamp();
    	double newLat = 0, newLong = 0, newElev = 0;
    	int newSize = 0;
    	double latestTimeStamp = 0;
    	
    	for (ContextSummary summ : allSummaries)
    	{
    		if (summ instanceof GroupContextSummary) continue;
    		TargetSighting sighting = HashMapContextSummaryInterface.retrieveTargetSighting(newSummary, currentGroupSummary.getId(), newSummary.getId());
    		if (sighting==null || sighting.targetLocation ==null) continue;
    		Location loc = sighting.targetLocation.location;
    		if (loc==null) continue;
    		newLat += loc.latitude();
    		newLong += loc.longitude();
    		newElev += loc.elevation();
    		newSize++;
    		latestTimeStamp = Math.max(sighting.timestamp, latestTimeStamp);
    	}
    	   	
    	
    	if (newSize > 0){
    	Location newLoc = new Location (newLat/newSize, newLong/newSize, newElev/newSize);
    	newLocationStamp.location = newLoc;
    	newLocationStamp.timestamp = latestTimeStamp;
    	HashMapContextSummaryInterface.insertLocationStamp((HashMapContextSummary)currentGroupSummary, "target", newLocationStamp);
    	}
//    	TargetSighting oldSighting = HashMapContextSummaryInterface.retrieveTargetSighting(currentGroupSummary, currentGroupSummary.getId(), newSummary.getId());
//    	
//    	if (newSighting !=null && (oldSighting == null || newSighting.timestamp > oldSighting.timestamp))
//    	{
//        	HashMapContextSummaryInterface.insertTargetSighting((HashMapContextSummary)currentGroupSummary, newSighting);
//    	}
//    
//    	LocationStamp currentTargetLoc = HashMapContextSummaryInterface.retrieveLocationStamp(currentGroupSummary, "target");
//    	currentGroupSummary.
    }
    
}
