package pharoslabut.robotperimeter;

import pharoslabut.navigate.Location;
import edu.utexas.ece.mpc.context.summary.ContextSummary;
import edu.utexas.ece.mpc.context.summary.GroupContextSummary;
import edu.utexas.ece.mpc.context.summary.HashMapContextSummary;
import edu.utexas.ece.mpc.context.summary.HashMapGroupContextSummary;


public class NaiveRunningAverageTargetGroupDefinition extends InstantaneousAverageTargetGroupDefinition {

	public NaiveRunningAverageTargetGroupDefinition(int gId) {
		super(gId);
	}
	
	//aggregation: same as instantaneous, except group summaries are merged by naively averaging their instantaneous average, weighted
	//by the number of members in the group
	
    
    public void handleGroupSummary (GroupContextSummary currentGroupSummary,
            ContextSummary newGroupSummary)
    {
    	//average the two target locations (new one with instantaneous average), weight by number of members
    
    if (! (currentGroupSummary instanceof HashMapGroupContextSummary)) return; //TODO
    	
		LocationStamp currLoc = HashMapContextSummaryInterface
				.retrieveLocationStamp(currentGroupSummary, "target");
		LocationStamp otherLoc = HashMapContextSummaryInterface
				.retrieveLocationStamp(newGroupSummary, "target");

		int size = currentGroupSummary.get("groupSize");
		int othSize = newGroupSummary.get("groupSize");

		Location newLocation = new Location(
				(currLoc.location.latitude() * size + otherLoc.location.latitude()
						* othSize)
						/ (size + othSize), (currLoc.location.longitude()
						* size + otherLoc.location.longitude() * othSize)
						/ (size + othSize), 0);
		LocationStamp newLocstamp = new LocationStamp(newLocation,
				(long) Math.max(currLoc.timestamp, otherLoc.timestamp));
		HashMapContextSummaryInterface.insertLocationStamp(
				(HashMapContextSummary) currentGroupSummary, "target",
				newLocstamp);
    }
  

}
