package pharoslabut.robotperimeter;

import java.util.Set;

import pharoslabut.logger.Logger;
import edu.utexas.ece.mpc.context.group.LabeledGroupDefinition;
import edu.utexas.ece.mpc.context.summary.ContextSummary;
import edu.utexas.ece.mpc.context.summary.GroupContextSummary;
import edu.utexas.ece.mpc.context.summary.HashMapGroupContextSummary;

public class SamplesTargetGroupDefinition extends TargetGroupDefinition {

	public SamplesTargetGroupDefinition(int gId) {
		super(gId);
	}
	
	//aggregation:  the latest target sightings from each robot are stored in the group summary. When a group summary is received,
	//the target sightings are extracted and merged into the local group summary. 
	
    public void handleContextSummary(GroupContextSummary currentGroupSummary,
            ContextSummary newSummary)
    {
        super.handleContextSummary(currentGroupSummary, newSummary); 
        //current TargetGroupDefinition matches desired Samples handleContextSummary implementation 
            
    }
    
    public void handleGroupSummary (GroupContextSummary currentGroupSummary,
            ContextSummary newGroupSummary)
    {
   	 //get member ids
//    	// retrive target sightings for each of those id's :             TargetSighting sighting = HashMapContextSummaryInterface.retrieveTargetSighting(myTargetGroup,
//      myTargetGroup.getId(),
//      hostId); 	
  	//add those sightings to the local group
  	//calculate new target location for local group
    	
    	if (! (newGroupSummary instanceof GroupContextSummary)) return; //TODO check if condition makes sense
    		
    	Set<Integer> newGroupMembers = ((GroupContextSummary)newGroupSummary).getMemberIds();
    	
    	for (Integer gId : newGroupMembers)
    	{
    		TargetSighting sighting = HashMapContextSummaryInterface.retrieveTargetSighting(newGroupSummary,
                    newGroupSummary.getId(),
                    gId);
    		if (sighting!=null)
    			TargetGroupUtils.addTargetSighting(sighting, (HashMapGroupContextSummary)currentGroupSummary); //TODO for typecast
    		//TODO take into account that addTargetSighing also removes sightings older than 5 seconds
    	}
    	
        int groupSize = currentGroupSummary.getMemberIds().size();
        int radius = groupSize *3 + 0;
        Logger.log("putting groupRadius: " + radius + " and groupSize: " + groupSize);
        ((HashMapGroupContextSummary)currentGroupSummary).put("groupRadius", radius); //TODO typecast
        ((HashMapGroupContextSummary)currentGroupSummary).put("groupSize", groupSize);
    	 
    }
  

}
