package pharoslabut.robotperimeter;

import pharoslabut.logger.Logger;
import pharoslabut.navigate.Location;
import edu.utexas.ece.mpc.context.group.LabeledGroupDefinition;
import edu.utexas.ece.mpc.context.summary.ContextSummary;
import edu.utexas.ece.mpc.context.summary.GroupContextSummary;
import edu.utexas.ece.mpc.context.summary.HashMapContextSummary;
import edu.utexas.ece.mpc.context.summary.HashMapGroupContextSummary;

public class TargetGroupDefinition extends LabeledGroupDefinition {

    public TargetGroupDefinition(int gId) {
        super(gId);
    }

    @Override
    
	//aggregation implementation: combination of samples and naive running average. Samples are included in the
    //target group that is sent out and is used to calculate the local view of the target. However, when a new group is received
    //the target locations are just averaged - target sightings are not merged as they would be in a true samples implementation.

    
    public void handleContextSummary(GroupContextSummary currentGroupSummary,
                                     ContextSummary newSummary) {
    	
        super.handleContextSummary(currentGroupSummary, newSummary);


        if (newSummary == null)
            return;
        Logger.log("entered addLocalSummary with Summary id " + newSummary.getId());

        try{
        boolean isTargetSource = HashMapContextSummaryInterface.retrieveBoolean(newSummary, "isTargetSource");
        if (isTargetSource){
        	LocationStamp loc = HashMapContextSummaryInterface.retrieveLocationStamp(newSummary, "ownLocation");
        	if (newSummary.get("targetNumber")==null) return;
        	int targetId = newSummary.get("targetNumber");
//        	Logger.log("received message from Target " + targetId);
        	Vision.receivedTargetSignal(loc.location, targetId);
        	return;
        }
        }
        catch(Exception e)
        {
        	Logger.log(e.toString());
        }

        HashMapContextSummaryInterface.insertLocationStamp((HashMapGroupContextSummary)currentGroupSummary,
                                                           "hostLocationId" + newSummary.getId(),
                                                           HashMapContextSummaryInterface.retrieveLocationStamp(newSummary,
                                                                                                                "ownLocation"));

        TargetSighting sighting = HashMapContextSummaryInterface.retrieveTargetSighting(newSummary,
                                                                                        currentGroupSummary.getId(),
                                                                                        newSummary.getId());

        Logger.log("sighting retrieved: " + sighting);
//        Logger.log("newSummary retrieved sighting from: " + newSummary);

        TargetGroupUtils.addTargetSighting(sighting, (HashMapGroupContextSummary)currentGroupSummary);
        int groupSize = currentGroupSummary.getMemberIds().size();
        int radius = groupSize *3 + 0;
        Logger.log("putting groupRadius: " + radius + " and groupSize: " + groupSize);
        ((HashMapGroupContextSummary)currentGroupSummary).put("groupRadius", radius);
        ((HashMapGroupContextSummary)currentGroupSummary).put("groupSize", groupSize);
    
    }

    @Override
    public void handleGroupSummary(GroupContextSummary currentGroupSummary,
                                   ContextSummary newGroupSummary) {

        Logger.log("in handleGroupSummary");

        super.handleGroupSummary(currentGroupSummary, newGroupSummary);

   //     Logger.log("entered addTargetGroupContextSummary");
        LocationStamp currLoc = HashMapContextSummaryInterface.retrieveLocationStamp(currentGroupSummary,
                                                                                     "target");
        double size = currentGroupSummary.getMemberIds().size();
        LocationStamp otherLoc = HashMapContextSummaryInterface.retrieveLocationStamp(newGroupSummary,
                                                                                      "target");
        double othSize = (newGroupSummary).get("groupSize");
        // TODO taking the average works for 2 nodes, but duplicates will start appearing depending on the communication
        // with 3+ nodes
        Location newLocation = new Location(
                                            (currLoc.location.latitude() * size + otherLoc.location.latitude()
                                                                                  * othSize)
                                                    / (size + othSize),
                                            (currLoc.location.longitude() * size + otherLoc.location.longitude()
                                                                                   * othSize)
                                                    / (size + othSize), 0);
        LocationStamp newLocstamp = new LocationStamp(newLocation,
                                                      (long) Math.max(currLoc.timestamp,
                                                                      otherLoc.timestamp));
        HashMapContextSummaryInterface.insertLocationStamp((HashMapContextSummary) currentGroupSummary,
                                                           "target", newLocstamp);

    }
}
