package pharoslabut.robotperimeter;

import java.util.List;

import edu.utexas.ece.mpc.context.ContextHandler;
import edu.utexas.ece.mpc.context.summary.GroupContextSummary;
import edu.utexas.ece.mpc.context.summary.HashMapContextSummary;
import edu.utexas.ece.mpc.context.summary.HashMapGroupContextSummary;
import pharoslabut.logger.Logger;
import pharoslabut.navigate.Location;

public class TargetIntelligence extends Intelligence {
	
	private boolean isStatic;
	private int targetNumber;
	private ContextHandler handler;
		
	public TargetIntelligence(boolean isStatic, int targetNumber, int maxSpeed)
	{
		handler = ContextHandler.getInstance();
		this.maxSpeed = 1;//speed;
		this.isStatic = isStatic;
		this.targetNumber = targetNumber;
		
		mySummary = new HashMapContextSummary();
		
		HashMapContextSummaryInterface.insertLocationStamp(mySummary,
				"ownLocation", new LocationStamp(new Location(0, 0), -1));
		
		HashMapContextSummaryInterface.storeBoolean(mySummary, "isTargetSource", true);
		mySummary.put("targetNumber", this.targetNumber);
		
		Intelligence.myId = mySummary.getId();
		
		handler.updateLocalSummary(mySummary);
		
		Logger.log(String
				.format("TargetIntelligence Created; id: %d, targetNumber: %d",
						myId, targetNumber));
	}
	
	public void updateLocationKnowledge(Location pos){

		Logger.log("updating location knowledge");
		List<GroupContextSummary> groups = handler.getGroupSummaries();

		current = pos;

		LocationStamp ownPosition = new LocationStamp();
		if (current!=null){
		ownPosition.location = current;
		ownPosition.timestamp = System.currentTimeMillis();

		HashMapContextSummaryInterface.insertLocationStamp(mySummary,
				"ownLocation", ownPosition);
		handler.updateLocalSummary(mySummary);
		}
	
		Logger.log(String.format("updated data in intelligence; ownLocation: %s", ownPosition.toString()));		
	}
	
	public Location determineNewGoPosition()
	{
		if (isStatic) return current;
		goal = new Location (current.latitude() + (Math.random()-.5)*.0001, current.longitude() + (Math.random()-.5)*.0001);

		return 	goal;
	}
	
	public double setSpeed(){
		if (isStatic)	return 0;
		
		return Math.min(goal.distanceTo(current), maxSpeed);
	}
}