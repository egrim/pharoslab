package pharoslabut.robotperimeter.experiment;

import java.io.Serializable;

import pharoslabut.experiment.RobotExpSettings;

public class RobotPerimeterExpRobotSettings extends RobotExpSettings implements Serializable {

    private static final long serialVersionUID = -2869481034446214185L;

    private int numTargets;
    private boolean targetTracker;
    private boolean staticTarget;
    private double latitude;
    private double longitude;
    private double forceField;
    private boolean goToGoal;
    private boolean isTargetSource;
    private int targetNumber;
    private boolean expTargetSource;

	private double finalPositionTolerance = 5;

    public int getNumTargets() {
        return numTargets;
    }

    public void setNumTargets(int numTargets) {
        this.numTargets = numTargets;
    }

    public boolean isTargetTracker() {
        return targetTracker;
    }
    public double getForceField(){
    	return forceField;
    }
    public boolean getGoToGoal(){
    	return goToGoal;
    }
    

    public void setTargetTracker(boolean targetTracker) {
        this.targetTracker = targetTracker;
    }

    public boolean isStaticTarget() {
        return staticTarget;
    }
    
    public void setExpTargetSource(boolean val) {
        this.expTargetSource = val;
    }

    public boolean getExpTargetSource() {
        return expTargetSource;
    }

    public void setStaticTarget(boolean staticTarget) {
        this.staticTarget = staticTarget;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }
    public void setGoToGoal(boolean goToGoal)
    {
    	this.goToGoal = goToGoal;
    }
    public void setForceField(double forceField)
    {
    	this.forceField = forceField;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    public void setFinalPositionTolerance(double tolerance)
    {
    	this.finalPositionTolerance  = tolerance;
    }

	public double getFinalPositionTolerance() {
		return this.finalPositionTolerance;
	}
    public void setIsTargetSource(boolean val)
    {
    	this.isTargetSource  = val;
    }

	public boolean getIsTargetSource() {
		return this.isTargetSource;
	}
	
    public void setTargetNumber(int val)
    {
    	this.targetNumber  = val;
    }

	public int getTargetNumber() {
		return this.targetNumber;
	}
	

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(RobotPerimeterExpRobotSettings.class.getName());
        sb.append(" (");
        sb.append("numTargets=" + numTargets);
        sb.append(" targetTracker=" + targetTracker);
        sb.append(" staticTarget=" + staticTarget);
        sb.append(" latitude=" + latitude);
        sb.append(" longitude=" + longitude);
        sb.append(" forceField=" + forceField);
        sb.append(" goToGoal=" + goToGoal);
        sb.append(" finalPositionTolerance=" + finalPositionTolerance);
        
        sb.append(")");

        return sb.toString();
    }
}
