package pharoslabut.robotperimeter.experiment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import pharoslabut.RobotIPAssignments;

/**
 * Reads the configuration of an experiment. This is done by the base station
 * when it launches an experiment.
 * 
 * @author Chien-Liang Fok
 */
public class RobotPerimeterExpConfigReader {

	public static RobotPerimeterExpConfig readExpConfig(String fileName) {
		RobotPerimeterExpConfig result = new RobotPerimeterExpConfig();
		BufferedReader input = null;
		try {
			input = new BufferedReader(new FileReader(fileName));
		} catch (IOException ex) {
			ex.printStackTrace();
			System.err.println("Unable to open " + fileName);
			System.exit(1);
		}

		try {
			String line = null;
			int lineno = 1;
			while ((line = input.readLine()) != null) {
				if (!line.equals("") && !line.startsWith("//")) {
					if (line.contains("START_INTERVAL")) {
						String[] elem = line.split("[\\s]+");
						try {
							result.setStartInterval(Integer.valueOf(elem[1]));
						} catch (Exception e) {
							e.printStackTrace();
							System.err.println("Warning: Syntax error on line "
									+ lineno + " of experiment script "
									+ fileName + ":\n" + line);
							System.exit(1);
						}
					} else if (line.contains("EXP_NAME")) {
						String[] elem = line.split("[\\s]+");
						try {
							result.setExpName(elem[1]);
						} catch (Exception e) {
							e.printStackTrace();
							System.err.println("Warning: Syntax error on line "
									+ lineno + " of experiment script "
									+ fileName + ":\n" + line);
							System.exit(1);
						}
					} else if (line.contains("FAKE_LATITUDE")) {
						String[] elem = line.split("[\\s]+");
						try {
							result.setLatitude(Double.valueOf(elem[1]));
						} catch (Exception e) {
							e.printStackTrace();
							System.err.println("Warning: Syntax error on line "
									+ lineno + " of experiment script "
									+ fileName + ":\n" + line);
							System.exit(1);
						}
					} else if (line.contains("FAKE_LONGITUDE")) {
						String[] elem = line.split("[\\s]+");
						try {
							result.setLongitude(Double.valueOf(elem[1]));
						} catch (Exception e) {
							e.printStackTrace();
							System.err.println("Warning: Syntax error on line "
									+ lineno + " of experiment script "
									+ fileName + ":\n" + line);
							System.exit(1);
						}
					} else if (line.contains("FORCE_FIELD")) {
						String[] elem = line.split("[\\s]+");
						try {
							result.setForceField(Double.valueOf(elem[1]));
						} catch (Exception e) {
							e.printStackTrace();
							System.err.println("Warning: Syntax error on line "
									+ lineno + " of experiment script "
									+ fileName + ":\n" + line);
							System.exit(1);
						}
					} else if (line.contains("POSITION_TOLERANCE")) {
						String[] elem = line.split("[\\s]+");
						try {
							result.setFinalPositionTolerance(Double
									.valueOf(elem[1]));
						} catch (Exception e) {
							e.printStackTrace();
							System.err.println("Warning: Syntax error on line "
									+ lineno + " of experiment script "
									+ fileName + ":\n" + line);
							System.exit(1);
						}
					}
						else if (line.contains("TARGET_SOURCE"))
						{
							String[] elem = line.split("[\\s]+");
							try {
								result.setExpTargetSource(Boolean
										.valueOf(elem[1]));
							} catch (Exception e) {
								e.printStackTrace();
								System.err.println("Warning: Syntax error on line "
										+ lineno + " of experiment script "
										+ fileName + ":\n" + line);
								System.exit(1);
							}							
					
					} else if (line.contains("ROBOT")) {
						try {
							String[] elem = line.split("[\\s]+");
							RobotPerimeterExpRobotSettings rs = new RobotPerimeterExpRobotSettings();
							rs.setName(elem[1]);
							rs.setIPAddress("10.11.12."
									+ RobotIPAssignments.getID(rs.getName()));
							rs.setPort(7776); // Just assume default
							boolean isTargetSource = Boolean.valueOf(elem[2]);
							rs.setIsTargetSource(isTargetSource);
							if (isTargetSource){
								rs.setTargetNumber(Integer.valueOf(elem[3]));
								rs.setLatitude(Double.valueOf(elem[4]));
								rs.setLongitude(Double.valueOf(elem[5]));
							}
							else {
								rs.setNumTargets(Integer.valueOf(elem[3]));
								rs.setTargetTracker(Boolean.valueOf(elem[4]));
								rs.setStaticTarget(Boolean.valueOf(elem[5]));
								rs.setForceField(result.getForceField());
								rs.setFinalPositionTolerance(result
										.getFinalPositionTolerance());
								rs.setExpTargetSource(result.getExpTargetSource());
								rs.setGoToGoal(Boolean.valueOf(elem[6]));
								if (rs.getGoToGoal()) {
									rs.setLatitude(Double.valueOf(elem[7]));
									rs.setLongitude(Double.valueOf(elem[8]));
								} else {
									rs.setLatitude(result.getLatitude());
									rs.setLongitude(result.getLongitude());
								}
							}
							result.addRobot(rs);
						} catch (Exception e) {
							e.printStackTrace();
							System.err.println("Warning: Ignoring line "
									+ lineno + " of experiment script "
									+ fileName);
						}
					}
				}
				lineno++;
			}
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
