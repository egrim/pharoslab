package pharoslabut.robotperimeter.experiment;

import pharoslabut.experiment.ExpConfig;

public class RobotPerimeterExpConfig extends ExpConfig {

    private double latitude;
    private double longitude;
    private double forceField;
    private double finalPositionTolerance;
    private boolean targetSource;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    public double getForceField() {
        return forceField;
    }

    public void setForceField(double forcefield) {
        this.forceField = forcefield;
    }
    public void setFinalPositionTolerance(double tolerance)
    {
    	this.finalPositionTolerance = tolerance;
    }
    public double getFinalPositionTolerance()
    {
    	return finalPositionTolerance;
    }
    public void setExpTargetSource(boolean val)
    {
    	this.targetSource = true;
    }
    public boolean getExpTargetSource()
    {
    	return targetSource;
    }
    


}
