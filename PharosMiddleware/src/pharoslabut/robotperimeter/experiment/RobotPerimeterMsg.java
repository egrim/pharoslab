package pharoslabut.robotperimeter.experiment;

import pharoslabut.io.Message;

public class RobotPerimeterMsg implements Message {

    private static final long serialVersionUID = -7430859630279152105L;

    private RobotPerimeterExpRobotSettings robotSettings;

    public RobotPerimeterMsg(RobotPerimeterExpRobotSettings robotSettings) {
        this.robotSettings = robotSettings;
    }

    public RobotPerimeterExpRobotSettings getRobotSettings() {
        return robotSettings;
    }

    @Override
    public MsgType getType() {
        return MsgType.LOAD_ROBOT_PERIMETER_CONFIG;
    }

}
