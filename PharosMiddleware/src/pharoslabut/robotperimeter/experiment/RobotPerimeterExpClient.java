package pharoslabut.robotperimeter.experiment;

import pharoslabut.experiment.ExpType;
import pharoslabut.experiment.RobotExpSettings;
import pharoslabut.io.SetTimeMsg;
import pharoslabut.io.StartExpMsg;
import pharoslabut.io.TCPMessageSender;
import pharoslabut.logger.Logger;

public class RobotPerimeterExpClient {

	/**
     * The connection to the PharosExpServer.
     */
    private TCPMessageSender sender = TCPMessageSender.getSender();

    /**
     * The constructor.
     *
     * @param expConfigFileName The name of the file containing the experiment configuration.
     * @see pharoslabut.experiment.ExpConfig
     * @see pharoslabut.navigate.motionscript.MotionScriptReader
     */
	public RobotPerimeterExpClient(String expConfigFileName) {
        RobotPerimeterExpConfig expConfig = RobotPerimeterExpConfigReader.readExpConfig(expConfigFileName);
		try {
			Logger.log("Setting local time on each robot...");
			for (int i=0; i < expConfig.numRobots(); i++) {
				RobotExpSettings currRobot = expConfig.getRobot(i);
				Logger.log("\tSending SetTimeMsg to " + currRobot.getName());

				SetTimeMsg msg = new SetTimeMsg();
				sender.sendMessage(currRobot.getIPAddress(), currRobot.getPort(), msg);
			}

			Logger.log("Sending each robot the experiment parameters...");
			for (int i=0; i < expConfig.numRobots(); i++) {
                RobotPerimeterExpRobotSettings currRobot = (RobotPerimeterExpRobotSettings) expConfig.getRobot(i);
				Logger.log("\tSending motion script to robot " + currRobot.getName());

				RobotPerimeterMsg msg = new RobotPerimeterMsg(currRobot);
				sender.sendMessage(currRobot.getIPAddress(), currRobot.getPort(), msg);
			}

			// Pause to ensure each robot receives their motion script.
			// This is to prevent out-of-order messages.
			int startTime = 5;
			Logger.log("Starting experiment in " + startTime + "...");
			while (startTime-- > 0) {
				synchronized(this) {
					try {
						wait(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				if (startTime > 0) Logger.log(startTime + "...");
			}


			// Send each robot the start experiment command.
			int delay = 0;

			StartExpMsg sem = new StartExpMsg(expConfig.getExpName(), ExpType.RUN_ROBOT_PERIMETER, delay);

			for (int i=0; i < expConfig.numRobots(); i++) {
				RobotExpSettings currRobot = expConfig.getRobot(i);

				Logger.log("Sending StartExpMsg to robot " + currRobot.getName() + "...");
				sender.sendMessage(currRobot.getIPAddress(), currRobot.getPort(), sem);

				// Update the delay between each robot.
				delay += expConfig.getStartInterval();

				//sem.setDelay(delay);
                sem = new StartExpMsg(expConfig.getExpName(), ExpType.RUN_ROBOT_PERIMETER, delay);
			}
		} catch(Exception e) {
			Logger.logErr("Problem while communicating with the robots...");
			e.printStackTrace();
		}
	}

	private static void print(String msg) {
		System.out.println(msg);
	}

	private static void usage() {
		print("Usage: " + RobotPerimeterExpClient.class.getName() + " <options>\n");
		print("Where <options> include:");
		print("\t-file <experiment configuration file name>: The name of the file containing the experiment configuration (required)");
		print("\t-debug: enable debug mode");
	}

	public static void main(String[] args) {
		String expConfigFileName = null;

		try {
			for (int i=0; i < args.length; i++) {

				if (args[i].equals("-file")) {
					expConfigFileName = args[++i];
				}
				else if (args[i].equals("-debug") || args[i].equals("-d")) {
					System.setProperty ("PharosMiddleware.debug", "true");
				}
				else {
					System.setProperty ("PharosMiddleware.debug", "true");
					print("Unknown parameter: " + args[i]);
					usage();
					System.exit(1);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			usage();
			System.exit(1);
		}

		if (expConfigFileName == null) {
			usage();
			System.exit(1);
		}

		print("Exp Config: " + expConfigFileName);
		print("Debug: " + ((System.getProperty ("PharosMiddleware.debug") != null) ? true : false));

		new RobotPerimeterExpClient(expConfigFileName);
	}
}
