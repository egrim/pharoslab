package pharoslabut.robotperimeter;

import java.util.Map;
import java.util.TreeMap;

import pharoslabut.logger.Logger;
import pharoslabut.navigate.Location;
import edu.utexas.ece.mpc.context.summary.ContextSummary;
import edu.utexas.ece.mpc.context.summary.GroupContextSummary;
import edu.utexas.ece.mpc.context.summary.HashMapContextSummary;
import edu.utexas.ece.mpc.context.summary.HashMapGroupContextSummary;

public class TargetGroupUtils {

    public static int timeThreshold = 5 * 1000; // (ms) disregard sightings older than this number of seconds

    public static Map<Integer, LocationStamp> retrieveAllLocalLocations(HashMapGroupContextSummary myTargetGroup) {
        Map<Integer, LocationStamp> all = new TreeMap<Integer, LocationStamp>();
        for (Integer g: myTargetGroup.getMemberIds()) {
            all.put(g, HashMapContextSummaryInterface.retrieveLocationStamp(myTargetGroup,
                                                                            "hostLocationId"
                                                                                    + g.intValue()));
        }
        return all;
    }

    public static void addTargetSighting(TargetSighting sight, HashMapGroupContextSummary myTargetGroup) {
        Logger.log("Entered addTargetSighting");
        if (sight == null)
            return;
        // remove target Sightings older than 5 seconds
        Integer[] memberIds = new Integer[myTargetGroup.getMemberIds().size()];
        myTargetGroup.getMemberIds().toArray(memberIds);

        for (int i = 0; i < memberIds.length; i++) {
            int hostId = memberIds[i];
            TargetSighting sighting = HashMapContextSummaryInterface.retrieveTargetSighting(myTargetGroup,
                                                                                            myTargetGroup.getId(),
                                                                                            hostId);
            if (sighting == null)
                continue;
            if (sighting.timestamp < System.currentTimeMillis() - timeThreshold) {
                HashMapContextSummaryInterface.removeTargetSighting(myTargetGroup, hostId);
            }
        }

        TargetSighting oldSighting = HashMapContextSummaryInterface.retrieveTargetSighting(myTargetGroup,
                                                                                           myTargetGroup.getId(),
                                                                                           sight.hostId);
        Logger.log("comparing old and new timestamps: " + sight.timestamp + " "
                   + System.currentTimeMillis() + " " + timeThreshold);
        if (sight.timestamp > System.currentTimeMillis() - timeThreshold
            && (oldSighting == null || sight.timestamp > oldSighting.timestamp)) // newer or first sighting from given
                                                                                 // host
        {
            HashMapContextSummaryInterface.insertTargetSighting(myTargetGroup, sight);
            calculateBestTargetGuess(myTargetGroup);
        }
    }

    private static void calculateBestTargetGuess(GroupContextSummary myTargetGroup) {
        // best guess for intercept
        Logger.log("entered calculateBestTargetGuess");
        double targetTimeStampLatest = 0;
        double latitude = 0;
        double longitude = 0;

        // ArrayList<TargetSighting> allSightings = new ArrayList<TargetSighting>();
        int size = 0;
        Logger.log("members in targetgroup: " + myTargetGroup.getMemberIds().toString());
        for (int id: myTargetGroup.getMemberIds()) {
            TargetSighting s = HashMapContextSummaryInterface.retrieveTargetSighting(myTargetGroup,
                                                                                     myTargetGroup.getId(),
                                                                                     id);
            if (s != null) {
                latitude += s.targetLocation.location.latitude();
                longitude += s.targetLocation.location.longitude();
                if (s.timestamp > targetTimeStampLatest)
                    targetTimeStampLatest = s.timestamp;
                size++;
                // allSightings.add(s);
            }
        }

        latitude /= size;
        longitude /= size;
        LocationStamp targetLocation = new LocationStamp();
        targetLocation.timestamp = targetTimeStampLatest;

        targetLocation.location = new Location(latitude, longitude);

        HashMapContextSummaryInterface.insertLocationStamp((HashMapContextSummary) myTargetGroup,
                                                           "target", targetLocation);
    }
}


