#!/usr/bin/perl -w

@files = <*.log>;
foreach $file (@files) {
my $output = "gps" . $file;
open (MYOUTPUT, '>'.$output);
open (MYINPUT, '<'.$file);
print MYOUTPUT "latitude, longitude, name\n";
	my $count = 1;
    foreach $line (<MYINPUT>){
	if (index($line, 'Target Location') != -1)
	{
	    my $gps = $line;
	    my @locations =  $line =~ m/\((.*)\)/;
	    print MYOUTPUT join(",", @locations).", point ". $count . "\n";
	    $count++;
#	    print MYOUTPUT $line;
	}
    }
}
 
